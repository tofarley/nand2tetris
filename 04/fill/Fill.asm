// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.

(BEGIN)
@KBD    // reference keyboard
D=M     // store value of keyboard state
@DRAW   // load the label for draw
D;JNE   // jump to @DRAW if D != 0 (i.e. keyboard is pressed)
@CLEAR
0;JMP   // if we didn't go to @DRAW, the key isn't pressed. @CLEAR the screen

(DRAW)
@R1
M=-1    // Set R1 to 0xFF (-1) to color all pixels black
@UPDATE
0;JMP

(CLEAR)
@R1
M=0     // Set R1 to 0x00 to clear the pixels if the keyboard was released.
//@UPDATE
//0;JMP

(UPDATE)
@KBD
D=A

@SCREEN
D=D-A   // full screen memory is RAM[KEYBOARD] - RAM[SCREEN]
D=D-1   // subtract 1 so we don't overwrite our keyboard register

@R0     // the example code required a user to store the number
M=D     // of rows in R0. So we'll use that to store the total
        // length of our draw.

@R0
D=M
@n
M=D     // n = RAM[0]

@i
M=0     // i = 0

@SCREEN
D=A
@address
M=D     // address = 16384 (base address of the screen)

(LOOP)
@i
D=M
@n
D=D-M
@BEGIN
D;JGT   // if i > n goto the beginning and listen for a keypress

@R1     // The type of pixel to draw (black or white) is stored in R1
D=M

@address
A=M     // writing to memory using a pointer
M=D     // we write the color that was stored in R1 above

@i
M=M+1   // i = i + 1
//@32   // in the demo code, we were jumping 32 words ahead
//D=A   // this was to draw a column. we don't need that here
@address

//M=D+M // address = address + 32
M=M+1   // again, we're not advancing 32 words, so just add 1

@LOOP
0;JMP   // goto LOOP and keep drawing