// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// Put your code here.
@R2
M=0     // Make sure RAM[2] is 0 at start.

@R1
D=M
@DONE
D;JEQ

@i
M=D     // set a loop counter to the value in RAM[1]

(ADD)
@R0
D=M     // we'll leave one operand in D for fun

@R2
M=D+M   // Get our accumulated values from R2 and add D to it.
@i
MD=M-1   // decrement loop counter.
@DONE
D;JEQ   // if our loop counter is 0, we are done
@ADD
0;JMP

(DONE)
@DONE
0;JMP