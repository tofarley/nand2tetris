#!/usr/bin/env python3
import re
import os
import argparse

parser = argparse.ArgumentParser("HACK Assembler")
parser.add_argument("filename")
args = parser.parse_args()

filename = args.filename
#filename = 'add/Add.asm'
outfile = "%s.hack" % os.path.splitext(filename)[0] 

instructions = []
binary = []
labels = {}
variables = {}
curr_addr = 0
variable_addr = 16

symbols = {
  'R0':   '0',
  'R1':   '1',
  'R2':   '2',
  'R3':   '3',
  'R4':   '4',
  'R5':   '5',
  'R6':   '6',
  'R7':   '7',
  'R8':   '8',
  'R9':   '9',
  'R10':  '10',
  'R11':  '11',
  'R12':  '12',
  'R13':  '13',
  'R14':  '14',
  'R15':  '15',
  'SP':   '0',
  'LCL':  '1',
  'ARG':  '2',
  'THIS':  '3',
  'THAT':  '4',
  'SCREEN': '16384',
  'KBD':    '24576'
}

cbits = {
  "0":   0b0101010,
  "1":   0b0111111,
  "-1":  0b0111010,
  "D":   0b0001100,
  "A":   0b0110000,
  "!D":  0b0001101,
  "!A":  0b0110001,
  "-D":  0b0001111,
  "-A":  0b0110011,
  "D+1": 0b0011111,
  "A+1": 0b0110111,
  "D-1": 0b0001110,
  "A-1": 0b0110010,
  "D+A": 0b0000010,
  "D-A": 0b0010011,
  "A-D": 0b0000111,
  "D&A": 0b0000000,
  "D|A": 0b0010101,
  "M":   0b1110000,
  "!M":  0b1110001,
  "-M":  0b1110011,
  "M+1": 0b1110111,
  "M-1": 0b1110010,
  "D+M": 0b1000010,
  "D-M": 0b1010011,
  "M-D": 0b1000111,
  "D&M": 0b1000000,
  "D|M": 0b1010101
}

dbits = {
  "null": 0b000,
  "M":    0b001,
  "D":    0b010,
  "MD":   0b011,
  "A":    0b100,
  "AM":   0b101,
  "AD":   0b110,
  "AMD":  0b111
}

jbits = {
  "null": 0b000,
  "JGT":  0b001,
  "JEQ":  0b010,
  "JGE":  0b011,
  "JLT":  0b100,
  "JNE":  0b101,
  "JLE":  0b110,
  "JMP":  0b111
}

with open(filename, 'r') as file:
  lines = file.readlines()

for line in lines:
  # Remove all comments from file
  # Then remove ALL whitespace (even in a command)
  # also make all characters capital.
  cleaned = re.sub(r"\s+", "", line.split('//', 2)[0])
  if cleaned:
    if cleaned.startswith('('):
      labels[re.search(r'\((.*?)\)',cleaned).group(1)] = curr_addr
    else:
      curr_addr += 1

      

for line in lines:
  cleaned = re.sub(r"\s+", "", line.split('//', 2)[0])
  # If the line isn't empty, add it to our list of instructions
  if cleaned:
    if cleaned.startswith('('):
      # This is a label
      #labels[re.search(r'\((.*?)\)',cleaned).group(1)] = curr_addr
      pass
    elif cleaned.startswith('@') and cleaned[1:] in symbols:
      # This is a symbol
      curr_addr += 1
      thingy = "@%s" % symbols[cleaned[1:]]
      instructions.append(thingy)
      #instructions.append(symbols[cleaned[1:]])
    elif cleaned.startswith('@') and not cleaned[1:].isnumeric() and cleaned[1:] not in labels:
      if cleaned[1:] not in variables:
        # We're defining a new variable!
        variables[cleaned[1:]] = variable_addr & 0b0111111111111111
        variable_addr +=1
        curr_addr += 1
        instructions.append(cleaned)
      else:
        # Let's use an existing variable!
        curr_addr += 1
        #import pdb; pdb.set_trace()
        instructions.append("@%s" % str(variables[cleaned[1:]]))
    else:
      curr_addr += 1
      instructions.append(cleaned)

for instruction in instructions:
  if instruction.startswith('@'):
    if instruction[1:].isnumeric():
      # This is an immediate value
      # And this to ensure that the MSB is always 0, even if something goes wrong.
      a_instruction = int(instruction[1:]) & 0b0111111111111111
    else:
      if instruction[1:] in symbols:
        # This is a pre-defined symbol!
        a_instruction = symbols[instruction[1:]] & 0b0111111111111111
      elif instruction[1:] in variables:
        # This is a variable!
        a_instruction = variables[instruction[1:]] & 0b0111111111111111
      else:
        # This is a label!
        #import pdb; pdb.set_trace()
        a_instruction = labels[instruction[1:]] & 0b0111111111111111
    print(instruction.ljust(15) + " -> " + format(a_instruction, '016b'))
    binary.append(a_instruction)
  else:
    # C instruction

    # dest=comp;jump
    if "=" in instruction and ";" in instruction:
      dest, tmp = instruction.split('=', 2)
      comp, jump = tmp.split(';', 2)
    # dest=comp
    elif "=" in instruction and ";" not in instruction:
      dest, comp = instruction.split('=', 2)
      jump = "null"
    # comp;jump
    elif "=" not in instruction and ";" in instruction:
      comp, jump = instruction.split(';', 2)
      dest = "null"
    # huh?
    else:
      print('huh?')

    c_instruction = 0b1110000000000000
    c_instruction |= (cbits[comp] << 6)
    c_instruction |= (dbits[dest] << 3)
    c_instruction |= (jbits[jump])
    print(instruction.ljust(15) + " -> " + format(c_instruction, '016b'))
    binary.append(c_instruction)

with open(outfile, 'w') as file:
  for instruction in binary:
    file.write(format(instruction, '016b'))
    file.write("\n")

